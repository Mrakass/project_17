﻿#include <iostream>
#include <cmath>
using namespace std;

class Vector {
private: 
	double x, y, z;
public:
	Vector(double x1 = 0, double y1 = 0, double z1 = 0) {
		x = x1;
		y = y1;
		z = z1;
	}
	void info() {
		cout << "coordinates: (" << x << " " << y << " " << z << ")" << endl;
	}
	double Lenght() {
		return sqrt(x * x + y * y + z * z);
	}
};
int main() {
	Vector v1(41, 72, 73);
	v1.info();
	cout << "lenght: " << v1.Lenght() << endl;
	return 0;
}